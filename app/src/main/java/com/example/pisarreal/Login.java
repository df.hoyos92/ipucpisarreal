package com.example.pisarreal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pisarreal.Administrador.MenuAdmin;
import com.example.pisarreal.Entidades.Usuario;
import com.example.pisarreal.Usuario.MenuPrincipal;
import com.example.pisarreal.Usuario.Registro;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener {

    TextInputLayout tilUser, tilPassword;
    public static TextInputEditText tieUser, tiePassword;
    TextView tvRegistry;
    Button btLogin;
    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tilUser = findViewById(R.id.tilUsuario);
        tilPassword = findViewById(R.id.tilContraseña);
        tieUser = findViewById(R.id.tieUsuario);
        tiePassword = findViewById(R.id.tieContraseña);
        tvRegistry = findViewById(R.id.tvRegistro);
        tvRegistry.setPaintFlags(tvRegistry.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        btLogin = findViewById(R.id.btIniciarSesion);
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginWS();
            }
        });
        tvRegistry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registry = new Intent(getApplicationContext(), Registro.class);
                startActivity(registry);
            }
        });
        tieUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(tieUser.getText().toString().trim().isEmpty()){
                    tilUser.setError("Campo Vacio");
                }else {
                    tilUser.setError(null);
                }
            }
        });
        tiePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(tiePassword.getText().toString().trim().isEmpty()){
                    tilPassword.setError("Campo Vacio");
                }else {
                    tilPassword.setError(null);
                }
            }
        });
    }
    private void loginWS() {
        String url = "https://ipucpisarreal.000webhostapp.com/jsonConsulta.php?documento="+tieUser.getText().toString();
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                1000 * 20,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(jsonObjectRequest);
    }
    @Override
    public void onResponse(JSONObject response) {
        Usuario usuario = new Usuario();
        JSONArray jsonArray = response.optJSONArray("usuario");
        try {
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            usuario.setNumDocumento(jsonObject.optString("documento"));
            usuario.setContraseña(jsonObject.optString("contrasena"));
            usuario.setTipoUsuario(jsonObject.optInt("tipousuario"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String documento = usuario.getNumDocumento();
        String contraseña = usuario.getContraseña();
        int tipoUsuario = usuario.getTipoUsuario();
        if(documento.equals("No Registra")){
            Toast.makeText(this, "Usuario No Registrado", Toast.LENGTH_SHORT).show();
        } else if (contraseña.equals(tiePassword.getText().toString())){
            if(tipoUsuario == 0){
                Toast.makeText(this, "Inicio de Sesion Exitoso", Toast.LENGTH_SHORT).show();
                Intent user = new Intent(getApplicationContext(), MenuPrincipal.class);
                startActivity(user);
            }else if(tipoUsuario == 1){
                Toast.makeText(this, "Inicio de Sesion Exitoso", Toast.LENGTH_SHORT).show();
                Intent admin = new Intent(getApplicationContext(), MenuAdmin.class);
                startActivity(admin);
            }
        }else Toast.makeText(this, "Contraseña Incorrecta", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "No se pudo Consultar"+error.toString(), Toast.LENGTH_SHORT).show();
        System.out.println("ERROR ->" + error.toString());
    }



}