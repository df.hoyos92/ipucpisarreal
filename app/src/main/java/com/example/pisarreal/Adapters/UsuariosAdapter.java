package com.example.pisarreal.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pisarreal.Entidades.Usuario;
import com.example.pisarreal.R;

import java.util.ArrayList;
import java.util.List;

public class UsuariosAdapter extends RecyclerView.Adapter<UsuariosAdapter.ViewHolderUsuarios> {

    List<Usuario> listaUsuarios = new ArrayList<>();

    public UsuariosAdapter(ArrayList<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    @Override
    public ViewHolderUsuarios onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.usuarios_list,parent,false);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(layoutParams);
        return new ViewHolderUsuarios(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderUsuarios holder, int position) {
        holder.tvnameL.setText(listaUsuarios.get(position).getNombres());
        holder.tvlastnameL.setText(listaUsuarios.get(position).getApellidos());
        holder.tvtypedocumentL.setText(listaUsuarios.get(position).getTipoDocumento());
        holder.tvdocumentL.setText(listaUsuarios.get(position).getNumDocumento());
        holder.tvdateL.setText(listaUsuarios.get(position).getNacimiento());
        holder.tvphoneL.setText(listaUsuarios.get(position).getTelefono());

    }

    @Override
    public int getItemCount() {
        return listaUsuarios.size();
    }

    public class ViewHolderUsuarios extends RecyclerView.ViewHolder {

        TextView tvnameL, tvlastnameL, tvtypedocumentL, tvdocumentL, tvdateL, tvphoneL;
        public ViewHolderUsuarios(View itemView) {
            super(itemView);

            tvnameL = itemView.findViewById(R.id.tvNameL);
            tvlastnameL = itemView.findViewById(R.id.tvLastnameL);
            tvtypedocumentL = itemView.findViewById(R.id.tvdocumentL);
            tvdocumentL = itemView.findViewById(R.id.tvNumDocumentL);
            tvdateL = itemView.findViewById(R.id.tvDateL);
            tvphoneL = itemView.findViewById(R.id.tvPhoneL);
        }
    }
}
