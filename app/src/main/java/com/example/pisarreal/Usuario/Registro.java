package com.example.pisarreal.Usuario;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pisarreal.Login;
import com.example.pisarreal.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

public class Registro extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener{

    TextInputLayout tilName, tilLastName, tilNumDocument, tilDate, tilPhone, tilPassword;
    TextInputEditText tieName, tieLastName, tieNumDocument, tieDate, tiePhone, tiePassword;
    Spinner spTypeDocument;
    Button btRegis;
    String typeDocument;
    ArrayAdapter<CharSequence> typosDocument;
    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        tilName = findViewById(R.id.tilNombres);
        tieName = findViewById(R.id.tieNombres);
        tilLastName = findViewById(R.id.tilApellidos);
        tieLastName = findViewById(R.id.tieApellidos);
        tilNumDocument = findViewById(R.id.tilDocumento);
        tieNumDocument = findViewById(R.id.tieDocumento);
        tilDate = findViewById(R.id.tilNacimiento);
        tieDate = findViewById(R.id.tieNacimiento);
        tilPhone = findViewById(R.id.tilTelefono);
        tiePhone = findViewById(R.id.tieTelefono);
        tilPassword = findViewById(R.id.tilContraseñaR);
        tiePassword = findViewById(R.id.tieDContraseñaR);
        spTypeDocument = findViewById(R.id.spDocumento);
        btRegis = findViewById(R.id.btRegistro);
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        btRegis.setEnabled(false);
        typosDocument = ArrayAdapter.createFromResource(this,R.array.tipo_documento, android.R.layout.simple_spinner_dropdown_item);
        spTypeDocument.setAdapter(typosDocument);
        spTypeDocument.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeDocument =parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        tieName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(tieName.getText().toString().trim().isEmpty()){
                    tilName.setError("Campo Vacio");
                }else {
                    tilName.setError(null);
                    validarCampos();
                }
            }
        });
        tieLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(tieLastName.getText().toString().trim().isEmpty()){
                    tilLastName.setError("Campo Vacio");
                }else {
                    tilLastName.setError(null);
                    validarCampos();
                }
            }
        });
        tieNumDocument.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(tieNumDocument.getText().toString().trim().isEmpty()){
                    tilNumDocument.setError("Campo Vacio");
                }else {
                    tilNumDocument.setError(null);
                    validarCampos();
                }
            }
        });
        tieDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(tieDate.getText().toString().trim().isEmpty()){
                    tilDate.setError("Campo Vacio");
                }else {
                    tilDate.setError(null);
                    validarCampos();
                }
            }
        });
        tiePhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(tiePhone.getText().toString().trim().isEmpty()){
                    tilPhone.setError("Campo Vacio");
                }else {
                    tilPhone.setError(null);
                    validarCampos();
                }
            }
        });
        tiePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (tiePassword.getText().toString().isEmpty()) {
                    tilPassword.setError("Campo Vacio");
                } else if (validarTamContraseña(tiePassword.getText().toString()) == false) {
                    tilPassword.setError("Contraseña Menor a 8 Caracteres");
                } else if (validarContraseña(tiePassword.getText().toString()) == false) {
                    tilPassword.setError("La Contraseña Debe contener Minimo 1 Mayuscula y 2 Numeros");
                } else {
                    tilPassword.setError(null);
                    validarCampos();
                }
            }
        });
        btRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarWS();
            }
        });
    }

    private void validarCampos(){
        if(tieName.getText().toString().trim().isEmpty() || tieLastName.getText().toString().trim().isEmpty() ||
                tieNumDocument.getText().toString().trim().isEmpty() || tiePhone.getText().toString().trim().isEmpty()||
                tiePassword.getText().toString().trim().isEmpty() || (validarTamContraseña(tiePassword.getText().toString()) == false)||
                (validarContraseña(tiePassword.getText().toString()) == false) || tieDate.getText().toString().trim().isEmpty()){
            btRegis.setEnabled(false);
        }else btRegis.setEnabled(true);
    }
    private boolean validarTamContraseña(String contraseña){
        boolean rtn = true;
        if (contraseña.length() < 8) rtn = false;
        return rtn;
    }
    private boolean validarContraseña(String contraseña){
        boolean rtn = false;
        int mayuscula = 0;
        int numero = 0;
        for (int i = 0; i < contraseña.length(); i++) {
            char clave = contraseña.charAt(i);
            String passValue = String.valueOf(clave);
            if (passValue.matches("[A-Z]")) mayuscula++;
            else if(passValue.matches("[0-9]")) numero++;
        }
        if(mayuscula > 0 && numero > 2){
            rtn = true;
        }
        return rtn;
    }
    private void cargarWS(){
        String url="https://ipucpisarreal.000webhostapp.com/jsonRegistro.php?nombres="+tieName.getText().toString()+
                   "&apellidos="+tieLastName.getText().toString()+"&tipodocumento="+typeDocument+"" +
                   "&documento="+tieNumDocument.getText().toString()+"&nacimiento="+tieDate.getText().toString()+
                   "&telefono="+tiePhone.getText().toString()+"&contrasena="+tiePassword.getText().toString();
        url=url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);
    }
    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(this, "Registro Exitoso", Toast.LENGTH_SHORT).show();
        Intent registro = new Intent(getApplicationContext(), Login.class);
        startActivity(registro);
    }
    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, "No se pudo Registrar", Toast.LENGTH_SHORT).show();
    }
}