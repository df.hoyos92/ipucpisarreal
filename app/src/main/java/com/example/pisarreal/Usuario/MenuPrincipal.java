package com.example.pisarreal.Usuario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.pisarreal.EditarPerfilFragment;
import com.example.pisarreal.InformacionFragment;
import com.example.pisarreal.InicioFragment;
import com.example.pisarreal.Login;
import com.example.pisarreal.PerfilFragment;
import com.example.pisarreal.R;
import com.google.android.material.navigation.NavigationView;

public class MenuPrincipal extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;
    Fragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        drawerLayout = findViewById(R.id.drawerLayoutUser);
        navigationView = findViewById(R.id.navigationView);
        toolbar = findViewById(R.id.app_Bar);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Inicio");
        actionBar.setHomeAsUpIndicator(R.drawable.ic_baseline_menu_24);
        fragment = new InicioFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.contentMainUser,fragment).commit();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home:
                        fragment = new InicioFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.contentMainUser,fragment).commit();
                        drawerLayout.closeDrawer(GravityCompat.START);
                        actionBar.setTitle("Inicio");
                        return true;
                    case R.id.perfil:
                        fragment = new PerfilFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.contentMainUser,fragment).commit();
                        drawerLayout.closeDrawer(GravityCompat.START);
                        actionBar.setTitle("Perfil");
                        return true;
                    case R.id.horarios:
                        fragment = new HorariosFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.contentMainUser,fragment).commit();
                        drawerLayout.closeDrawer(GravityCompat.START);
                        actionBar.setTitle("Horarios");
                        return true;
                    case R.id.informacion:
                        fragment = new InformacionFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.contentMainUser,fragment).commit();
                        drawerLayout.closeDrawer(GravityCompat.START);
                        actionBar.setTitle("Informacion");
                        return true;
                    case R.id.close:
                        Intent close = new Intent(getApplicationContext(), Login.class);
                        startActivity(close);
                        finish();
                        return true;
                }
                return true;
            }
        });
    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return  true;
        }
        return super.onOptionsItemSelected(item);
    }
    public static void editPerfil(){

    }
}