package com.example.pisarreal.Usuario;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.pisarreal.EncuestaFragment;
import com.example.pisarreal.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HorariosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HorariosFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    TextInputLayout tilNameAsist, tilDocumentAsist, tilPhoneAsist;
    TextInputEditText tieNameAsist, tieDocumentAsist, tiePhoneAsist;
    TextView tvPuestos;
    Spinner spHorarios;
    Button btPoll, btConfirm, btCancel;
    Fragment fragment;

    public HorariosFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HorariosFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HorariosFragment newInstance(String param1, String param2) {
        HorariosFragment fragment = new HorariosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_horarios, container, false);
        tilDocumentAsist = view.findViewById(R.id.tilDocumentAsist);
        tilNameAsist = view.findViewById(R.id.tilNameAsist);
        tilPhoneAsist = view.findViewById(R.id.tilPhoneAsist);
        tieDocumentAsist = view.findViewById(R.id.tieDocumentAsist);
        tieNameAsist = view.findViewById(R.id.tieNameAsist);
        tiePhoneAsist = view.findViewById(R.id.tiePhoneAsist);
        tvPuestos = view.findViewById(R.id.tvPuestos);
        spHorarios = view.findViewById(R.id.spHorarios);
        btPoll = view.findViewById(R.id.btPoll);
        btConfirm = view.findViewById(R.id.btConfirm);
        btCancel = view.findViewById(R.id.btCancel);
        btPoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                poll();
            }
        });
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clean();
            }
        });
        btConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
        return view;
    }

    private void save() {
    }

    private void clean() {
    }

    private void poll() {
        fragment = new EncuestaFragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.contentMainUser,fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}