package com.example.pisarreal.Administrador;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.pisarreal.InicioFragment;
import com.example.pisarreal.Login;
import com.example.pisarreal.R;
import com.example.pisarreal.PerfilFragment;
import com.example.pisarreal.InformacionFragment;
import com.google.android.material.navigation.NavigationView;

public class MenuAdmin extends AppCompatActivity {

    private DrawerLayout drawerLayoutAdmin;
    private NavigationView navigationViewAdmin;
    private Toolbar toolbar;
    Fragment fragmentAdmin = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_admin);

        drawerLayoutAdmin = findViewById(R.id.drawerLayoutAdmin);
        navigationViewAdmin = findViewById(R.id.navigationViewAdmin);
        toolbar = findViewById(R.id.app_Bar);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Inicio");
        actionBar.setHomeAsUpIndicator(R.drawable.ic_baseline_menu_24);
        fragmentAdmin = new InicioFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.contentMainAdmin, fragmentAdmin).commit();

        navigationViewAdmin.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home:
                        fragmentAdmin = new InicioFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.contentMainAdmin, fragmentAdmin).commit();
                        drawerLayoutAdmin.closeDrawer(GravityCompat.START);
                        actionBar.setTitle("Inicio");
                        return true;
                    case R.id.perfil:
                        fragmentAdmin = new PerfilFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.contentMainAdmin, fragmentAdmin).commit();
                        drawerLayoutAdmin.closeDrawer(GravityCompat.START);
                        actionBar.setTitle("Perfil");
                        return true;
                    case R.id.listarUsuario:
                        fragmentAdmin = new ListarUsuarioFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.contentMainAdmin, fragmentAdmin).commit();
                        drawerLayoutAdmin.closeDrawer(GravityCompat.START);
                        actionBar.setTitle("Usuarios Registrados");
                        return true;
                    case R.id.horarios:
                        fragmentAdmin = new HorariosAdminFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.contentMainAdmin, fragmentAdmin).commit();
                        drawerLayoutAdmin.closeDrawer(GravityCompat.START);
                        actionBar.setTitle("Horarios");
                        return true;
                    case R.id.listaReserva:
                        fragmentAdmin = new ListarReservasFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.contentMainAdmin, fragmentAdmin).commit();
                        drawerLayoutAdmin.closeDrawer(GravityCompat.START);
                        actionBar.setTitle("Reservas");
                        return true;
                    case R.id.informacion:
                        fragmentAdmin = new InformacionFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.contentMainAdmin, fragmentAdmin).commit();
                        drawerLayoutAdmin.closeDrawer(GravityCompat.START);
                        actionBar.setTitle("Informacion");
                        return true;
                    case R.id.close:
                        Intent close = new Intent(getApplicationContext(), Login.class);
                        startActivity(close);
                        finish();
                        return true;

                }
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                drawerLayoutAdmin.openDrawer(GravityCompat.START);
                return  true;
        }
        return super.onOptionsItemSelected(item);
    }
}
