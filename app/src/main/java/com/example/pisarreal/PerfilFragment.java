package com.example.pisarreal;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pisarreal.Entidades.Usuario;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PerfilFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PerfilFragment extends Fragment{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ImageView ivfoto;
    TextView tvnombre, tvapellidos, tvtdocumento, tvndocumento, tvnacimiento, tvtelefono;
    Button btedit ;
    Fragment fragment;
    RequestQueue requestQueue;
    Integer tipousuario = 0;
    JsonObjectRequest jsonObjectRequest;

    public PerfilFragment() {
        // Required empty public constructor
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PerfilFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PerfilFragment newInstance(String param1, String param2) {
        PerfilFragment fragment = new PerfilFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil, container, false);
        ivfoto = view.findViewById(R.id.ivphoto);
        tvnombre = view.findViewById(R.id.tvNameP);
        tvapellidos = view.findViewById(R.id.tvLastnameP);
        tvtdocumento = view.findViewById(R.id.tvdocumentP);
        tvndocumento = view.findViewById(R.id.tvNumDocumentP);
        tvnacimiento = view.findViewById(R.id.tvDateP);
        tvtelefono = view.findViewById(R.id.tvPhoneP);
        btedit = view.findViewById(R.id.btEdit);
        btedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editperfil();
            }
        });
        requestQueue = Volley.newRequestQueue(getContext());
        cargarWS();
        return view;
    }

    private void cargarWS() {
        String url = "https://ipucpisarreal.000webhostapp.com/jsonPerfilUsuario.php?documento="+ Login.tieUser.getText().toString();
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Usuario usuario = new Usuario();
                JSONArray jsonArray = response.optJSONArray("usuario");
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject = jsonArray.getJSONObject(0);
                    usuario.setNombres(jsonObject.optString("nombres"));
                    usuario.setApellidos(jsonObject.optString("apellidos"));
                    usuario.setTipoDocumento(jsonObject.optString("tipodocumento"));
                    usuario.setNumDocumento(jsonObject.optString("documento"));
                    usuario.setNacimiento(jsonObject.optString("nacimiento"));
                    usuario.setTelefono(jsonObject.optString("telefono"));
                    usuario.setTipoUsuario(jsonObject.optInt("tipousuario"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                tvnombre.setText(usuario.getNombres());
                tvapellidos.setText(usuario.getApellidos());
                tvtdocumento.setText(usuario.getTipoDocumento());
                tvndocumento.setText(usuario.getNumDocumento());
                tvnacimiento.setText(usuario.getNacimiento());
                tvtelefono.setText(usuario.getTelefono());
                tipousuario = usuario.getTipoUsuario();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "No se pudo Consultar", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

    public void editperfil(){
        fragment = new EditarPerfilFragment();
        if (tipousuario == 0) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.contentMainUser,fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }else {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.contentMainAdmin,fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }
}